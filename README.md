# Solvers and Generators

This site is the front-end for a number of different solver and generator animations such as mazes, Sudoku and Truchet tiles. 

https://solvergenerators.herokuapp.com/

![maze](maze.png)

![sudoku](sudoku.png)

![truchet](truchet.png)

```$ git clone https://gitlab.com/rileythomp14/solvers-generators```

```$ cd solvers-generators```

```$ ng serve```

